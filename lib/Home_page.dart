import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:purposblackmedia_app/Webview_home.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Webview(),
      ),
    );
  }
}
